#!/bin/bash
###
 # @Author         : mli
 # @Email          : mli@poptown.io
 # @Description    : 
 # @FilePath       : /scripts/ci_cd.sh
 # @Date           : 2024-01-26 18:12:39
 # @LastEditTime   : 2024-02-21 09:39:36
 # @LastEditors    : mli
### 
# ci/cd脚本
# 依次执行如下操作:
# 1. 修改pubspec.yaml文件，将 version: 一行的的形如 1.0.4+2 最后的build号递增1
# 2. git commit一个Bump Version
# 3. 在项目根目录下执行： ```flutter build apk --debug```生成deubg包
# 4. 在android目录下执行: ```fastlane beta``` 将上传蒲公英，通知飞书群

pubspec=../pubspec.yaml
echo 获取当前版本号
current_version=$(grep -o 'version: [0-9.]*+[0-9]*' $pubspec)
# echo $current_version
echo 提取build号
build_number=$(echo $current_version | grep -o '+[0-9]*' | grep -o '[0-9]*')
# echo $build_number
echo 递增build号
new_build_number=$((build_number + 1))
# echo $new_build_number
echo 替换版本号中的build号
new_version=$(echo $current_version | sed "s/+[0-9]*/+$new_build_number/")
echo new_version: $new_version
echo 替换pubspec.yaml中的版本号
sed -i '' -e "s/version: [0-9.]*+[0-9]*/$new_version/" $pubspec
echo 提交版本号变更
git commit -am "Bump Version"
echo 生成debug包
cd ..
flutter build apk --debug --target-platform android-arm64 --split-per-abi 
echo 上传蒲公英并通知飞书群
cd android
# 提取版本号
# 将类似version: 1.0.4+5提取为1.0.4(5)
version=$(echo $new_version | sed "s/version: \([0-9.]*\)+\([0-9]*\)/\1(\2)/")
# echo $version
fastlane beta version:$version