fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android beta

```sh
[bundle exec] fastlane android beta
```

上传APK到蒲公英, 上报飞书修改日志

### android upload

```sh
[bundle exec] fastlane android upload
```

上传蒲公英

### android change_logs

```sh
[bundle exec] fastlane android change_logs
```

获取修改日志

### android notify_changeLog

```sh
[bundle exec] fastlane android notify_changeLog
```

生成修改日志，将结果向飞书发送

### android notify_feishu

```sh
[bundle exec] fastlane android notify_feishu
```

向飞书发送信息, 信息从输入参数content字段传入

### android getChangeLogs

```sh
[bundle exec] fastlane android getChangeLogs
```

获取修改日志

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
